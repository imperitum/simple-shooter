// Fill out your copyright notice in the Description page of Project Settings.

#include <BehaviorTree/BlackboardComponent.h>
#include "AIController.h"
#include "BTTask_Shoot.h"
#include "ShooterCharacter.h"
#include <Kismet/GameplayStatics.h>

UBTTask_Shoot::UBTTask_Shoot()
{
	NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	if (!OwnerComp.GetAIOwner()) return EBTNodeResult::Failed;

	AShooterCharacter* PlayerPawn = Cast<AShooterCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (!PlayerPawn || PlayerPawn->IsDead()) return EBTNodeResult::Failed;

	AShooterCharacter* ThisCharacter = Cast<AShooterCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (!ThisCharacter) return EBTNodeResult::Failed;

	ThisCharacter->Fire();

	return EBTNodeResult::Succeeded;
}