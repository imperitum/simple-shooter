// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include <EngineUtils.h>
#include "ShooterAIController.h"

void AKillEmAllGameMode::PawnKilled(APawn* KilledPawn)
{
	Super::PawnKilled(KilledPawn);

	APlayerController* ShooterController = Cast<APlayerController>(KilledPawn->GetController());
	if (ShooterController)
	{
		GameEnd(false);
	}

	for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!AIController->IsDead())
		{
			return;
		}
	}

	GameEnd(true);
}

void AKillEmAllGameMode::GameEnd(bool bIsPlayerWinner)
{
	for (APlayerController* Controller : TActorRange<APlayerController>(GetWorld()))
	{
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
